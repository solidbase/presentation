FROM node:alpine

WORKDIR /usr/src/app

RUN apk update && apk add nodejs git
RUN npm install -g webpro/reveal-md

RUN mkdir -p /usr/lib/node_modules/reveal-md/node_modules/reveal.js/images

COPY . /usr/src/app/

#EXPOSE 5000 

CMD ["sh", "-c", "reveal-md slides.md --theme theme.css --port 5000"]
