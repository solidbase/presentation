---
title: FLOSS tools for SFS management
tags: solidbase, SFS, CSA, financial sustainability
description: A presentation about the most usable tools for SFS Management
revealOptions:
  width: "100%"
---

# FLOSS tools for SFS management

A summary of the software evaluation for the **SolidBase** project

----

### Free and Libre Open Source Software for Solidarity based Food Systems (SFS)

An evaluation of Software solutions for CSAs and alike within the Erasmus+ strategic partnership called **SolidBase** comprising 5 European organizations (TVE (Budapest), AMPI (Prague) , Agronauten (Freiburg), Urgenci (France) and SoLawi (Germany)) for the creation of an educational program for increasing financial sustainability of SFS

----

<div class="stretch">
[![TVE logo](./logos/tudatosvasarlo-logo4.png)](http://tudatosvasarlo.hu)
[![Ampi logo](./logos/logo-ampi.jpg)](http://asociaceampi.cz/)  

[![Agronauten logo](./logos/logo_agronauten.png)](http://www.agronauten.net/)
[![urgenci logo](./logos/logo-urgenci.png)](https://urgenci.net/)
[![solawi logo](./logos/logo_solawi.png)](https://www.solidarische-landwirtschaft.org)  

[![Erasmus+ logo](logosbeneficaireserasmusright_en.jpg)](https://ec.europa.eu/programmes/erasmus-plus/opportunities/strategic-partnerships-field-education-training-and-youth_en)

</div>

----

## Johannes Winter  
[solidbase@solidarische-landwirtschaft.org](mailto:solidbase@solidarische-landwirtschaft.org)

https://www.solidarische-landwirtschaft.org

### 13.11.2019

----

#### access this Presentation on:

https://slides.solidbase.info

**Source available on:**

https://lab.allmende.io/solidbase/presentation

#### The presentation is based on data gathered here: (WIP!)

https://cloud.solawi.allmende.io/s/5gq246GSw7axZCy

**created with**

https://github.com/webpro/reveal-md

---

## Why free Software?

<p class="fragment">[Free software keeps us in control of the technology we use](https://www.fsf.org/resources/resources/what-is-fs)</p>
<p class="fragment">[Without free software privacy is impossible](https://static.fsf.org/nosvn/posters/privacy-free-software-sign.svg)</p>
<p class="fragment">Free software development culture is more effective and sustainable. Easeness of translation and adaptability to local circumstances</p>
<p class="fragment">For sustainable transformative change we have to join the knowledge and forces of the progressive citizen souvereignity movements</p>
<p class="fragment">The leading digital enterprises (GAFAM -Google/Amazon/Facebook/Microsoft) are constantly struggling for a re-centralisation of the internet for maximising contol and thus profit - FLOSS and independend hosting would be a solution</p>

Note: This is a complex discussion. See latest surveillance scandals for details.

----

- [Open Organization principle](https://opensource.com/open-organization/resources/what-open-organization)
- [Free Software Foundation](https://www.fsf.org)
- [Free Software Foundation europe](https://fsfe.org)
- [Public money - public code!](https://publiccode.eu/)

Note: read more about free software
---

## Non-free solutions

- [facebook / REKO](https://sustainingroots.wordpress.com/2016/06/14/reko-rooted-governance-in-practice/)
- [The Food Assembly](http://thefoodassembly.com/en)
- https://www.harvie.farm/
- https://www.farmigo.com/
- https://kivalogic.com/
- https://www.pcgaertner.de/
- http://www.abobote.de/
- https://gemuese-anbauplaner.de/
- ...

---

## SFS taxonomy

### Two main strands of SFS can be distinguished

- **supply** orientated (CSA)
- **demand** orientated (Foodcoop)

Note: Supply orientated: The farm devides it's harvest among it's members which are in the most developed form owners.<br>Demand orientated: Like a normal shop, consumer retains full control.<br>SFS are an attempt to bring these two together, mainly it's about bringing more self determination to the producers.<br>A lot of different transformative business forms are summarized under this notion, and there are loads of mixed forms, and in each local strand there are elements from others

---

## European CSA varieties that developed Software

----

### [AMAP](http://miramap.org/)

Association pour le maintien d’une agriculture paysanne

- Cagette
- AmapJ
- [PressAmap](https://github.com/comptoirdesappli/amapress)
- [clic'Amap(?)](http://amap-aura.org/outil-de-gestion-amapk/)

Note: Association for Maintaining Small Scale Family Farming, sometimes also literally translated as Peasant Agriculture.<br>Later details on Cagette and AmapJ<br>PressAmap is a wordpress plugin which was in thorough rework lately.<br>The status of clicAmap is unknown, François Guitton wanted to come, but did not appear.<br>A AMAP equals to one depot, so one AMAP can get delivered by several farmers and farmers can have several AMAPs. These Softwares also allow for adjusting the current delivery to the daily needs of the consumer.

----

### [GASAP](https://gasap.be/)

Groupe d’achats solidaires de l’agriculture paysanne

- ~~RePanier~~

GASAP adapted the foodcoop software [Repanier](https://repanier.be) to CSA needs, but switched in 2019 to [cagette](https://cagette.net)

Note: Group of solidarity purchases for peasant agriculture. This is a buying group with long term commitments to the farmers using contracts.<br>Repanier had been developed as a foodcoop Software and is currently being adapted to the needs of GASAP

----

### [ACP](https://www.fracp.ch)

Agriculture Contractuelle de Proximité (Local Contract Agriculture)

- [Cake-ACP](https://gitlab.com/zpartakov/cakeACP/)
- [ACP-Admin](https://acp-admin.ch/)

Note: ACP is the French-Swiss CSA notion, Cake-ACP seems to be very promising as it is build upon dolibarr, a sophisticated ERP from France, but the maintainer, Fred Radeff is quite hard to reach

----

### [SoLawi](https://www.solidarische-landwirtschaft.org)

Solidarische Landwirtschaft

- Sunu / OpenOlitor
- Juntagrico

Note: Solidary agriculture. This two are Swiss developments, Sunu is the adaptation of OpenOlitor to german circumstances.

----

### Short comparison of CSA models

- ACP and SoLawi are quite similar to each other: Both describe a single farm with various delivery points.
- ACP more uses boxes, within SoLawi the bulk delivery with self-devision of the harvest by the members is more spread.
- As both are made for directly financing farms, the budget for running the agriculture is of central interest
- AMAP is more positioned on the consumers side. It describes the organisation that manages long term contracts between producers and consumers
- GASAP defines itself as a "brother" of AMAP

Note: Although the budget is of interest for the members, the bookkeeping itself is normally outsourced

---

## Food Coop Software

- [Open Food Network](https://openfoodnetwork.org/)
- [Foodsoft](https://github.com/foodcoops/foodsoft)
- [foodcoopshop](https://www.foodcoopshop.com/)
- [RePanier](https://repanier.be)
- [A comparison of the first 3 in German](https://datengarten.allmende.io/s/iHpogXeKCLZwESC)

Note: This are preordering systems for buying in bulk from local producers or whole sale.<br>OFN from Australia<br>Foodsoft from Amsterdam<br>foodcoopshop from Austria.<br>OFN is great because of it's big community and it's well done netowrk functionality, on the other side foodcoopshop has even implemented a distributed database to keep the stock of several hubs in sync.<br>Foodsoft has very well done product importing functionalities which make it well suited for standalone foodcoops (with lots of volunteering) for buying from gross sale.

---

## Other

- [Local Food Nodes](https://localfoodnodes.org/)

Note: This very new Software from Sweden is mainly a appointment app for consumers and producers. Products are ordered and paid beforehand.

---

## Communication / Online collaboration
Ask your local IT collective for hosting possibilities!  
Lists of independent hosters:
- https://github.com/libresh/awesome-librehosters
- https://riseup.net/de/security/resources/radical-servers
- [Chatons collective of independent hosters](https://chatons.org/en/chatons-collective-independant-transparent-open-neutral-and-ethical-hosters-providing-floss-based)


Note: For building a community around the SFS, communication is THE tool to use. Best work eye-to-eye meetings, but online collaboration becomes more and more important.

----

### E-Mail / E-Maillists

- [framasoft](https://wiki.framasoft.org/presentation_en)
- https://disroot.org
- https://systemausfall.org/
- https://mailchimp.com/ (commercial all in one provider)
- ...

For regular users a [client](https://www.thunderbird.net/) is very useful!  
For private communication [encryption](https://www.enigmail.net/) should be normal!

Note: Emailing is still a very useful tool for group coordination. It's outreach is unmet, and it's technical structure is distributed, what means democratic, if you don't use gmail, or united internet (gmx/web.de) email adresses.

----

### Forums / E-Maillist replacement

**https://www.discourse.org/**  
Civilized discussions


Amenities:
+ Quick creation of discussion rooms (categories)
+ Quick definition of the level of notifications for each topic

Drawbacks:
- A bit technical in appearance (uses a MarkDown editor)
- For first time users it is *not* very easy to define the level of notifications the user should reach,

Note: Some love it, some hate it. First Solawis (Weinheim / Malu) could be observed of only using it.

----

### Messengers

The new generation next generation of messengers (after *whatsup?* and *telegram*) comes **distributed** and **encrypted**.

- [Rocketchat](https://rocket.chat)/[Fairchat](https://fairchat.net)
- [Matrix](https://matrix.org/)/[Allmende riot instance](https://riot.allmende.io)
- [Signal](https://signal.org)

Note: email is now 40 years old and is not very popular with the smartphone using kids...<br>Fairchat is a branch of rocketchat for better UX for transformative initiatives<br>Signal is Edward Snowdens Favorite

----

### Online collaboration / file-exchange

**https://nextcloud.com/**  
Data sharing on the next level

- File exchange
- Shared Contacts
- Shared Calendar
- Writing texts together (LibreOfficeOnline / OnlyOffice)
- Online conferencing

For taking minutes on the fly [etherpads](https://text.allmende.io/) are still viable.  
The Markdown lover uses [HackMD](https://hack.allmende.io/).

----

### The extended Content management Systems

**https://www.drupal.org**

[![Gartencoop internal menu](./CMS/Screenshot_2018-07-17 Ernte- und Lieferstatistik Kooperative GartenCoop Freiburg.png)](http://www.gartencoop.org/tunsel/node/195)

Note:  Content Management Systems can be extended to have *internal space* on the website.

----

**https://www.drupal.org/project/openatrium**

[![Co-munity CSA freudenthal](./CMS/Screenshot_2018-07-17 CSA Freudenthal co-munity.png)](https://co-munity.net/csa/freudenthal)

Note: Or to allow for complex interactivity with others. Pitily these two projects are unmaintained at the moment.<br>Other very good examples of Solawi admin environments build in drupal have been created by Solawi Marburg and Kartoffelkombinat (both non OS yet)

----

### [WordPress](https://wordpress.com/)

Great CMS with hundreds of plugins. Of special interest are

- https://buddypress.org/
- [s2Member](https://wordpress.org/plugins/s2member/)

which can generate a forum like environment, accessible only to members

Note: These bloated CMS often do not have the very best user experience. Our advise is to use the tools that had been developed only for fulfilling this special task you need.

---

### Accounting / Enterprise Ressource Planning

- [GnuCash](https://www.gnucash.org/)
- [ERPNext](https://erpnext.com/)
- [dolibarr](https://www.dolibarr.org/)
- [Odoo](https://www.odoo.com/) / [coopiteasy](http://coopiteasy.be/)

Note: The world of ERPs is quite complex, and hundreds of proprietary solutions exist.<br>Gnucash is the only one that (almost) only allow for accounting.<br>ERPs are for managing your entire business.<br>the dedicated SFS solutions are sometimes called community ressource planning or network RPs<br>ERPNExt seems to be the freshest with the most developed understanding of community.

---

<!-- .slide: data-background="blue" -->

## User stories / epics

Note: What is the main purpose of a administrative software from my point of view (Solawista, gardener, coordinator, consumer).<br>These are more quite general epics from which the implemented functionalities originate from.

----

<!-- .slide: data-background="blue" -->

## CSA

- I need a system that helps me to devide the harvest among the members of the CSA
- It should easily output delivery lists for each delivery place
- Coordination of compulsory co-worker / volunteers should be possible in a distributed manner

Note: The primary user story was the only one where a calculator is used in the fields at my local Solawi. Interestingly only one tool has a solution for that: ACP-Admin

----

<!-- .slide: data-background="blue" -->

## SFS

- Members need to get information about the products in the store
- Members should be able to order products in the shopfront
- It should be possible to add a variable fee to the products or to each memebrship

Note: This is fulfilled quite well by all of the tools. More attention should be put on the socioecomic background (the community) of the tools when selecting one.

----

<!-- .slide: data-background="blue" -->

## Both

- An easy way to communicate with the members should be available
- I'd like to keep track of payments and expenses

---

<!-- .slide: data-background="blue" -->

## Analysed tools

- Juntagrico (**JA**)
- OpenOlitor / Sunu
- ACP-Admin
- Cagette
- AmapJ
- RePanier
- LocalFoodNodes (**LFN**)
- OpenFoodNetwork (**OFN**)



---

<!-- .slide: data-background="blue" -->

## Common functionality

----

<!-- .slide: data-background="blue" -->

Functionality         | JA | Sunu | Cagette | OFN | ACP-Admin| AMAPJ
----------------------|----|------|---------|-----|----------|------
Membership management | x  |   x  |    x    |  x  |     x    |  x
Email members         | x  |   x  |    x    |  x  |     x    |  x
Delivery planning     | x  |   x  |    x    |  x  |     x    |  x
Share management      | x  |   x  |    x    |     |     x    |  x

Note: Share management ~ contract management

----

<!-- .slide: data-background="blue" -->

Functionality         | JA | Sunu | Cagette | OFN | ACP-Admin| AMAPJ
----------------------|----|------|---------|-----|----------|------
Variable Orders       |    |      |    x    |  x  |          |  x
Control of payments   |  x |   x  |    x    |  x  |     x    |  x
SEPA generation       |    |   x  |         |     |          |
Volunteer Management  |  x |  (x) |         |     |    x     |  

---

<!-- .slide: data-background="blue" -->

## Structure of analysis

- Technosocial Background
- Screenshots
- Amenities
- Shortfalls


---

<!-- .slide: data-background="#44a647" -->

## [Juntagrico.org](https://juntagrico.org/)

<p class="fragment">created by ["Regionale Gartenkooperative  Ortoloco"](https://www.ortoloco.ch)  
<br>
based next to Zurich  
<br>
~400 members  
<br>
compulsory co-work coordination for all members well integrated into web tool juntagrico  
<br>
agricultural cooperation shares managable online  
<br>
Advanced bookkeeping in development
</p>
----

<!-- .slide: data-background="#44a647" -->


### codebase

- Python
- Django
- 1 main contributor
- Translation possible, but currently only in use by this farm in Switzerland

https://github.com/juntagrico/juntagrico

----

<!-- .slide: data-background="#44a647" -->

```python
class JuntagricoAppConfig(AppConfig):
    name = 'juntagrico'

    def ready(self):
        modules = load_modules()

        admin_menu_templates = []

        load_config('admin_menu_template',admin_menu_templates,modules)

        set_admin_menus(admin_menu_templates)
```

----

<!-- .slide: data-background="#44a647" -->

<div class="stretch">
<a href="http://www.fyrobemusig.ch/">
<img src="./juntagrico/Screenshot-2018-6-6%20ortoloco%20ch%20-%20Die%20regionale%20Gartenkooperative.png" alt="Website Design">
</a>
</div>

Note: Music plays a central part during the comunity work days in ortoloco

----

<!-- .slide: data-background="#44a647" -->

<div class="stretch">
<a href="https://juntagrico-demo.herokuapp.com">
<img src="./juntagrico/Screenshot-2018-6-6%20my%20juntagrico_admin_menu.png" alt="Administrative menu">
</a>
</div>

Note: Admin Menu with display of two subscription requests

----

<!-- .slide: data-background="#44a647" -->

<a href="https://juntagrico-demo.herokuapp.com">
<img src="./juntagrico/Screenshot-2018-6-6%20my%20juntagrico_abo_%C3%BCbersicht.png" alt="Overview of subscription details">
</a>

Note: Overview of subscriptions

----

<!-- .slide: data-background="#44a647" -->

<div class="stretch">
<a href="https://juntagrico-demo.herokuapp.com">
<img src="./juntagrico/Screenshot-2018-6-6%20my%20juntagrico_job_registration.png" alt="compulsive co-work registration">
</a></div>

Note: This is the status page of a mandatory co-work commitment. **the** core functionality of JA

----

<!-- .slide: data-background="#44a647" -->

<div class="stretch">
<a href="https://juntagrico-demo.herokuapp.com">
<img src="./juntagrico/Screenshot-2018-6-6%20my%20juntagrico_job_list.png" alt="overview of jobs">
</a></div>

Note: Overview of co-work commitments

----

<!-- .slide: data-background="#44a647" -->

<div class="stretch">
<a href="https://juntagrico-demo.herokuapp.com">
<img src="./juntagrico/Screenshot-2018-6-6%20my%20juntagrico_send_mails.png" alt="mail sending">
</a><div class="stretch">

Note: email is quite well integrated.

---

<!-- .slide: data-background="#e7e7e7" -->

## OpenOlitor / Sunu

<p class="fragment">http://sunu.eu/ is an organization that internationalizes the Swiss CSA Management software http://openolitor.org in a first step to German circumstances  
<br>
Quite professional fund-raising strategy, Crowdfunding, Google Impact challenge...  
<br>
Software development is mainly done by https://www.tegonal.com/  
<br>
A first round of testing, gathering impressions and implementing new functionality for Sunu completed at the beginning of this year
</p>
----

<!-- .slide: data-background="#e7e7e7" -->

- Translations to French, Spanish and English complete
- The translation to English haven't made it to the application at the time of this writing
- Translations are done on [crowdin](http://translate.openolitor.org/project/openolitor)

----

<!-- .slide: data-background="#e7e7e7" -->

### codebase

- https://github.com/OpenOlitor
- Sunu exists as a seperate branch there
- Server: Scala
- Client: AngularJS
- Translation of Software already done in some languages, but complete documentation only available in German

----

<!-- .slide: data-background="#e7e7e7" -->

```javascript
  def createKoerbe(lieferung: Lieferung)(implicit personId: PersonId, session: DBSession, publisher: EventPublisher): Lieferung = {
    logger.debug(s"Create Koerbe => lieferung : ${lieferung}")
    val vertriebList = vertriebInDelivery(lieferung.datum)
    val ret: Option[Option[Lieferung]] = stammdatenWriteRepository.getAbotypById(lieferung.abotypId) map { abotyp =>
      lieferung.lieferplanungId.map { lieferplanungId =>
        val abos: List[Abo] = stammdatenWriteRepository.getAktiveAbos(lieferung.abotypId, lieferung.vertriebId, lieferung.datum, lieferplanungId)
        val koerbe: List[(Option[Korb], Option[Korb])] = abos map { abo =>
          if (vertriebList.exists { vertrieb => vertrieb.id == abo.vertriebId }) {
            upsertKorb(lieferung, abo, abotyp)
          } else { (None, None) }
        }
        recalculateNumbersLieferung(lieferung)
      }
    }
    ret.flatten.getOrElse(lieferung)
  }
```
https://github.com/OpenOlitor/openolitor-server/blob/prod/src/main/scala/ch/openolitor/stammdaten/KorbHandler.scala

----

<!-- .slide: data-background="#e7e7e7" -->

```
      $scope.selectAbo = function(abo, itemId) {
        var allRows = angular.element('#abosTable table tbody tr');
        allRows.removeClass('row-selected');

        if ($scope.selectedAbo === abo) {
          $scope.selectedAbo = undefined;
        } else {
          $scope.selectedAbo = abo;
          var row = angular.element('#' + itemId);
          row.addClass('row-selected');
        }
      };
```
https://github.com/OpenOlitor/openolitor-client-admin/blob/sunu/app/scripts/kunden/detail/kundendetail.controller.js

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<a href="https://Sunuwwwtest.applicationcloud.io">
<img src="./openolitor/Screenshot-2018-6-6%20OpenOlitor.png">
</a></div>

Note: login screen

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<a href="https://sunuwwwtest.applicationcloud.io/admin/#/dashboard">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor_dashboard.png" alt="OpenOlitor Dashboard">
</a></div>

Note: Dash board. ToDo List, Invoices list, delivery planning

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<a href="https://sunuwwwtest.applicationcloud.io/admin/#/projektsettings">
<img src="./openolitor/Bildschirmfoto%20von%202018-06-11%2007-42-37_project%20configuration.png" alt="Project configuration">
</a></div>

Note: Project Configuration / Branding

----

<!-- .slide: data-background="#e7e7e7" -->

Direct access to database:
- csv imports
- database journal
- report generation with SQL commands

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<a href="https://sunuwwwtest.applicationcloud.io/admin/#/reports/1">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor_reports.png" alt="report generation directly from database commands">
</a></div>

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<a href="https://sunuwwwtest.applicationcloud.io/admin/#/depots/10001">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor_pickup_rooms.png" alt="pickup rooms configuration">
</a></div>

Note: Configuration of depots

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<a href="https://sunuwwwtest.applicationcloud.io/admin/#/abotypen/50000">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor_share%20types.png" alt="Definition of share types">
</a></div>

Note: Definition of subscription types

----

<!-- .slide: data-background="#e7e7e7" -->

<a href="https://sunuwwwtest.applicationcloud.io/admin/#/produzenten?q=">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor_producer.png" alt="Definition of producers">
</a>

Note: Configuration of producers

----

<!-- .slide: data-background="#e7e7e7" -->

<a href="https://sunuwwwtest.applicationcloud.io/admin/#/produkte">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor_products.png" alt="Definition of products">
</a>

Note: Configuration of products.<br>Products have no price

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<img src="./openolitor/Bildschirmfoto%20von%202018-06-11%2008-37-09_members_details.png" alt="members details">
</div>

Note: Members details, payment types direct debit= sepa

----

<!-- .slide: data-background="#e7e7e7" -->

<a href="https://sunuwwwtest.applicationcloud.io/admin/#/kunden/30012">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor%20individual%20price%20per%20share.png" alt="individual price per share">
</a>

Note: It's possible to set an individual price per subscription. This is a requirement for SoLawi as its solidarity principle is based upon the  possibility to freely set the individual price per share

----

<!-- .slide: data-background="#e7e7e7" -->

<a href="https://sunuwwwtest.applicationcloud.io/admin/#/pendenzen">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor_todo_manager.png" alt="todo manager">
</a>

Note: Details on ToDo list

----

<!-- .slide: data-background="#e7e7e7" -->

Delivery planning based on basket content
**but** easy overview of amounts the producers have to harvest/deliver

----

<!-- .slide: data-background="#e7e7e7" -->

<a href="https://sunuwwwtest.applicationcloud.io/admin/#/lieferplanung/10">
<img src="./openolitor/Screenshot-2018-6-11%20OpenOlito_delivery_planning.png" alt="delivery planning">
</a>

----

<!-- .slide: data-background="#e7e7e7" -->

<img src="./openolitor/Screenshot-2018-6-11%20OpenOlitor_delivery_planning_producer.png" alt="delivery planning producers view">


----

<!-- .slide: data-background="#e7e7e7" -->

Delivery planning box based.
**but** [Pickup-place based delivery notes possible with SQL](https://github.com/OpenOlitor/OpenOlitor/issues/17#issuecomment-396215782)

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<img src="./openolitor/Bildschirmfoto%20von%202018-06-11%2011-52-20_box_per_pickup.png" alt="delivery note per pickup room">
</div>

----

<!-- .slide: data-background="#e7e7e7" -->

<div class="stretch">
<img src="./openolitor/Bildschirmfoto%20von%202018-06-11%2012-29-50_sepa_export.png" alt="sepa export">
</div>

Note: Sepa exports via pain.008.001.07 XML

----

<!-- .slide: data-background="#e7e7e7" -->

[![Arbeitseinsätze](./openolitor/Bildschirmfoto von »2018-07-20 12-54-33«.png)](https://wwwtest.openolitor.ch/admin/#/arbeitsangebote)

Note: New feature in **OpenOlitor**: Arbeitseinsätze!


---

<!-- .slide: data-background="olive" -->

## [ACP-admin.ch](https://acp-admin.ch/)

<p class="fragment">Developed since 2013 out of the Swiss Agriculture Contractuelle de Proximité (ACP) [Rage de Vert](https://www.ragedevert.ch) near Neuchâtel.  
<br>
Lately used by five other ACPs: [Lumière des champs](http://www.lumiere-des-champs.ch/),  [TaPatate!](https://www.tapatate.ch) which is located in the German speaking part of Switzerland and some more  
<br>
english translation not yet ready, but would be easy to accomplish</p>

Note: But german translation is already available.

----

<!-- .slide: data-background="olive" -->

- Quite complete functionality:
	- Membershipmanagement
	- fully automatic invoice generation & bank account reconciliation using swiss ESR number
	- Elaborated delivery planning
	- Compulsory co-work coordination
	- Newsletter integration (with [mailchimp](https://mailchimp.com/))
- no open issues / feature requests

----

<!-- .slide: data-background="olive" -->

### codebase

- ruby on rails:

https://github.com/ragedevert/acp-admin

- ActiveAdmin framework:

https://activeadmin.info/


----

<!-- .slide: data-background="olive" -->

<div class="stretch">
[![ACP-Admin Dashboard](./ACP-Admin/Screenshot-2018-7-7 Übersicht Rage de Vert.png)](https://admin.ragedevert.ch/dashboard?locale=de)
</div>

Note: 1/2 Tage means "compulsory co-work days", Delivery planning is also done by velociped

----

<!-- .slide: data-background="olive" -->

![ACP-Admin full menu](./ACP-Admin/ACP-Admin_full_menu.png)

Note: Some more features hidden

----

<!-- .slide: data-background="olive" -->

<div class="stretch">
![ACP-Admin Delivery notes](./ACP-Admin/ACP-Admin_delivery_notes.png)
</div>


----

<!-- .slide: data-background="olive" -->

![ACP-Admin Box Content](./ACP-Admin/Screenshot-2018-7-7 Contenu Paniers Rage de Vert.png)

Note: Box content calculation.  \o/

----

<!-- .slide: data-background="olive" -->

![ACP-Admin Box sizes](./ACP-Admin/Screenshot-2018-7-7 Tailles panier Rage de Vert.png)

Note: These is the box size definition

----

<!-- .slide: data-background="olive" -->

![ACP-Admin absence organisation](./ACP-Admin/Screenshot-2018-7-7 Absences Rage de Vert.png)

Note: Organisation of the absent members from the ACP

----

<!-- .slide: data-background="olive" -->

![ACP-Admin depot list](./ACP-Admin/Screenshot-2018-7-7 Dépôts Rage de Vert.png)

Note: Definition of depots and price per delivery. Home delivery is an option, double costs

----

<!-- .slide: data-background="olive" -->

![ACP-Admin organization of "volunteers](./ACP-Admin/co-work organization.png)

Note: Organization of cumpulsory co-work (half days). Place is configurable, also for parties.

----
<!-- .slide: data-background="olive" -->

<div class="stretch">
![example invoice](./ACP-Admin/invoice-1114.png)
</div>

Note: Invoice generation and bank account reconcilation is said to work fully automatically as Thibaud said...

---

<!-- .slide: data-background="orange" -->

## Cagette.net

<p class="fragment">created by Bordeaux based [Alilo](https://www.alilo.fr/) *Activateur de circuits courts*  
<br>
Alilo offers training in it's usage esp. for it's non-free professional plugin **Cagette pro**  
<br>
communication and webmarketing also belongs to the trainigs programm  
<br>
These trainings are funded by the [Vivea training programm](https://www.vivea.fr/)  
<br>
Already in usage by >400 groups
</p>

----

<!-- .slide: data-background="orange" -->

- A 'Cagette' is a wooden box to carry vegetables
- French origin, but German and English already usable
- switch language with appending e.g. `?lang=en` to the URL
- Translations are done on [Weblate](https://hosted.weblate.org/projects/cagette/translations/)

----

<!-- .slide: data-background="orange" -->

### codebase

- [Haxe](https://haxe.org/)
    - exotic, but very intuitive and clean
    - usable in a utmost variable range of contexts
    - emerged out of the gaming industry
    - e.g. [Motion Twin](https://motion-twin.com/en/) is strategic partner
- lively [github community](https://github.com/bablukid/cagette)

----

<!-- .slide: data-background="orange" -->

```
package payment;

class Check extends payment.Payment
{
	public static var TYPE = "check";

	public function new()
	{
		var t = sugoi.i18n.Locale.texts;
		this.type = TYPE;
		this.icon = '<i class="fa fa-credit-card" aria-hidden="true"></i>';
		this.name = t._("Check");
		this.link = "/transaction/check";
	}

	public static function getCode(date:Date,place:db.Place,user:db.User){
		return date.toString().substr(0, 10) + "-" + place.id + "-" + user.id;
	}
}
```
https://github.com/bablukid/cagette/blob/master/src/payment/Check.hx

----

<!-- .slide: data-background="orange" -->

<div class="stretch">
<a href="https://app.cagette.net/?lang=en">
<img data-src="./cagette.net/Screenshot-2018-6-11%20Cagette%20net_start.png"></a>
</div>

Note: Guided tour through the application


----

<!-- .slide: data-background="orange" -->

<div class="stretch">
![Cagette groups on map](./cagette.net/upload_d12ad34430074cd7b0ddd82954e9629c.png)
</div>

Note: More than 400 active groups<br>But: Urgenci doesn't know them, and they did not appear in the survey.

----

<!-- .slide: data-background="orange" -->

<div class="stretch">
![Cagette group settings](./cagette.net/upload_ce26bf4e58a67a11c8aad9ada03d9bb9.png)
</div>

Note: Configuration of the group.<br>I haven't realized any changes with changing the group type.

----

<!-- .slide: data-background="orange" -->

![Cagette members list](./cagette.net/upload_56743cdbe71ba2572f58cff9cf3ea9d0.png)

Note: Members list. A lot can be done with csv exports

----

<!-- .slide: data-background="orange" -->

![Cagette possibilities for contrats](./cagette.net/Screenshot-2018-7-9 Cagette net.png)

Note: Two different varieties for contracts

----

<!-- .slide: data-background="orange" -->

<div class="stretch">
![Cagette contracts summary](./cagette.net/upload_6f0431a0995158c7944238a8b414450d.png)
</div>

Note: Summary of CSA style contract

----

<!-- .slide: data-background="orange" -->

![Cagette variable order style contract](./cagette.net/Screenshot-2018-7-9 Cagette net_variable_order.png)

Note: This is how you place orders on a variable order style contract.<br>Several pre configured product-categories available.

----

<!-- .slide: data-background="orange" -->

![Cagette contracts distributions](./cagette.net/upload_90c7e747fc44a159bc97ef52c12f09ad.png)

Note: Summary of distribution points

----

<!-- .slide: data-background="orange" -->

![Cagette Payment options](./cagette.net/upload_f38a389786fad0e1a9418f095c538538.png)

Note: payment options

----

<!-- .slide: data-background="orange" -->

![Cagette Paywall](./cagette.net/upload_bc0300dc32edf53fb4f8b41bf63e8784.png)

Note: Another view of payment options at members paywall checkout
----

<!-- .slide: data-background="orange" -->

![Cagette bank transfer key](./cagette.net/upload_979a6f83ecec39b9270ca6b23bdb179b.png)

Note: Payment backtracing is done via an reference number

----

<!-- .slide: data-background="orange" -->

<div class="stretch">
![Cagette payment management](./cagette.net/upload_8506ad1925dd854d6360f8c7a768f254.png)
</div>

Note: No automatic account reconciliation, everything is done manually

---

<!-- .slide: data-background="#D5B453" -->

## [AmapJ.fr](http://amapj.fr/)
<p class="fragment">AmapJ is a computer application to simplify the management of baskets and contract  
<br>
This software was initially developed for the needs of an AMAP of the Drôme. Now it is freely available  
<br>
Started in 2013  
<br>
About 160 Amap groups in France are using it
</p>

----

<!-- .slide: data-background="#D5B453" -->

### codebase

- Java on Tomcat
- The one and only maintainer is [Emanuel Brun](http://amapj.fr/auteur.html)
- Sourcecode available as [download of a zip ball](http://amapj.fr/download.html)

----

<!-- .slide: data-background="#D5B453" -->


#### Great and easily understandable documentation available

http://amapj.fr/docs_utilisateur.html

Note: From here are the screenshots

----

<!-- .slide: data-background="#D5B453" -->

<div class="stretch">
![Producer delivery list](./AmapJ/large11.png)
</div>

Note: Some examples of views. This is the display for a producer of the delivery list.<br>See contracts, deliveries, payments

----

<!-- .slide: data-background="#D5B453" -->

<div class="stretch">
![Consumer refinement of contract per delivery](./AmapJ/large02.png)
</div>

Note: The consumer can refine the contract to a single product, here types of bread

----

<!-- .slide: data-background="#D5B453" -->

<div class="stretch">
![Consumer list of checks](./AmapJ/large04.png)
</div>

Note: For each contract a list of amounts to be paid per check is generated

----

<!-- .slide: data-background="#D5B453" -->

<div class="stretch">
![Another check list view](./AmapJ/large09.png)
</div>

Note: another view of the checks to pay


----

<!-- .slide: data-background="#D5B453" -->

<div class="stretch">
![Amapiens list view](./AmapJ/large12.png)
</div>

Note: It is optionally possible to list all fellow amapiens and also to send an email to all.

---

<!-- .slide: data-background="#ccba89" -->

## [LocalFoodNodes.org](https://localfoodnodes.org/)

<p class="fragment">very fresh software from Sweden  
<br>
focused on transparent finances  
<br>
completely donation based  
<br>
appointments and preordering system for farmers and consumers  
</p>


----

<!-- .slide: data-background="#ccba89" -->

## Codebase

- PHP / laravel
- Javascript
- Mobile: React Native

----

<!-- .slide: data-background="#ccba89" -->

![nice map](./LFN/Screenshot-2018-7-7 Local Food Nodes.png)

----

<!-- .slide: data-background="#ccba89" -->

<div class="stretch">
![nice products](./LFN/Screenshot-2018-7-7 Lammskinn - Mellan, korthårigt, vitt.png)
</div>

----

<!-- .slide: data-background="#ccba89" -->

<div class="stretch">
![easy creation of nodes](./LFN/Screenshot-2018-7-7 Create node - Local Food Nodes.png)
</div>

---

<!-- .slide: data-background="azure" -->

## [OpenFoodNetwork.org](https://openfoodnetwork.org)

<p class="fragment">Global network started in Australia in 2012  
<br>Shortening of the food distribution chain by fostering food hubs for local produce  
<br>
Decentrally organized with mostly independend national / regional chapters  
<br>
Regular global community and administrative calls. Global budget for development  
(The idea comes first, technology is secondary)
</p>

Note: This is an ongoing discussion in the discourse. This opens the possibility to map all sorts of solutions for shortening the Local Food Supply chain under a meta umbrella organisation.<br>But it also leeds to a tendency of weakening the consequent use of OpenSource. Pattern of "the end sanctifies the means".

----

<!-- .slide: data-background="azure" -->

- The manual is now also available in French:

https://guide.openfoodnetwork.org/

- About 50 memners are active in the global community on:

https://community.openfoodnetwork.org/

Note: More direct communication with the organizers and developers is happening on Slack.

----

<!-- .slide: data-background="azure" -->

### codebase

- Ruby on rails: https://github.com/openfoodfoundation/openfoodnetwork
- [Spree Commerce](https://spreecommerce.org/) as Framework for the shop functionality / product management
- a handful developers, which partly get their work compensated from global budgets.
- See the comparison of [OpenFoodNetwork, FoodCoopShop and Foodsoft](https://datengarten.allmende.io/s/iHpogXeKCLZwESC )

Note: As OFN is a fork of Spree, whith some relevant changes upgrades to major releases of Spree make quite an effort

----

<!-- .slide: data-background="azure" -->

<div class="stretch">
[![OFN UK map](./OFN/upload_f836c06316e794702cba4382b69451be.png)](https://www.openfoodnetwork.org.uk/map)
</div>
Note: Quite nice map, would have a lot of possibilities to include other Management tools into it.

----

<!-- .slide: data-background="azure" -->

[![OFN Demo Hub Producers](./OFN/Screenshot-2018-6-26 OFN UK Demo Hub.png)](https://www.openfoodnetwork.org.uk/ofn-uk-test-hub/shop)

Note: The naming shops vs hubs

----

<!-- .slide: data-background="azure" -->

[![OFN fish shop](./OFN/Screenshot-2018-6-26 Sole of Discretion.png)](https://www.openfoodnetwork.org.uk/sole-of-discretion/shop)

Note: The possibilities of food to sell on OFN are endless

----

<!-- .slide: data-background="azure" -->

<div class="stretch">
[![OFN Tamar Valley](./OFN/Screenshot-2018-6-26 Tamar Valley Food Hub.png)](https://www.openfoodnetwork.org.uk/tamar-valley-food-hubs/shop)
</div>

Note: A lot of things in OFN can be managed using tags: type of food, distance

----

<!-- .slide: data-background="azure" -->

[![OFN Tamar Valley Order Cycle](./OFN/Screenshot-2018-6-26 Tamar Valley Food Hub order cycle.png)](https://www.openfoodnetwork.org.uk/tamar-valley-food-hubs/shop)

Note: Order Cycles are the basic economic temporal unit OFN uses

----

<!-- .slide: data-background="azure" -->

[![OFN Admin Dashboard](./OFN/Screenshot-2018-6-26 Overview - OFN Administration.png)](https://www.openfoodnetwork.org.uk/admin)

Note: *Spree* Dashboard, Order Cycles

----

<!-- .slide: data-background="azure" -->

[![OFN Admin Hub categories](./OFN/Screenshot-2018-7-9 Enterprises - OFN Administration.png)](https://www.openfoodnetwork.org.uk/admin/enterprises)

Note: 3 different main categories of Hubs are possible: Profile only, Producer shop, Producer hub. This might be the entry point to use OFN as umbrella for the other tools.

----

<!-- .slide: data-background="azure" -->

<div class="stretch">
<a href="https://www.openfoodnetwork.org.uk/admin/reports">
![OFN Reports](./OFN/Screenshot-2018-6-26 Reports - OFN Administration.png)
</a></div>

Note: Mailinglist and delivery list is quite rough, unformattet excel, but templates for beautifying could be self-made. Xero export (bookkeeping), other export formats in the dev-pipe

----

<!-- .slide: data-background="azure" -->

<div class="stretch">
[![OFN Order Cycles](./OFN/Screenshot-2018-6-26 Order Cycles - OFN Administration.png)](https://www.openfoodnetwork.org.uk/admin/order_cycles)
</div>

Note: OFNs system of order cycles is the base of all

----

<!-- .slide: data-background="azure" -->

<div class="stretch">
[![OFN E2E Relationships](./OFN/Screenshot-2018-6-26 Enterprise relationships - OFN Administration.png)](https://www.openfoodnetwork.org.uk/admin/enterprise_relationships)
</div>

Note: All data has only to be entered once into the system. With E2E Relationships it can be copied from shop to the next.

----

<!-- .slide: data-background="azure" -->

[![OFN Shipping methods](./OFN/Screenshot-2018-6-26 Shipping Methods - OFN Administration.png)](https://www.openfoodnetwork.org.uk/admin/shipping_methods/new)

Note: Definition of delivery location, and shipping methods. Possibility to put fees on top of each delivery

----

<!-- .slide: data-background="azure" -->

<div class="stretch">
[![OFN payment methods](./OFN/Screenshot-2018-6-26 Payment Methods - OFN Administration.png)](https://www.openfoodnetwork.org.uk/admin/payment_methods/new)
</div>
Note: CreditCards, PayPal, Stripe and others fully implemented. Fees are possible to put on top of each payment.

---

<!-- .slide: data-background="#00686b" -->

## [RePanier.be](https://repanier.be)
<p class="fragment">Development from the bruxxels based GASAP network  
<br>
Pre ordering system for up to 3 month in advance, products are paid in bulk  
<br>
elaborated accounting / balance system for members payments  
<br>
~8 active consumer groups, 1 active producer using it  
<br>
one developer  
<br>
active community
</p>

----

<!-- .slide: data-background="#00686b"; color="white" -->

### codebase

- https://github.com/pcolmant/repanier
- Django CMS 3.7 / Bootstrap 3 / Python 3.5.
- [Django CMS cascade](https://github.com/jrief/djangocms-cascade)
- http://www.django-rest-framework.org/
- interesting functionality for keeping track of members and producers account balance

----

<!-- .slide: data-background="#00686b" -->

![Re Panier Contracts](./RePanier/repanier_contrat.jpg)

Note: Contracts from several producers with delivery dates

----

<!-- .slide: data-background="#00686b" -->

![Re Panier generation of order list](./RePanier/offre_prep.png)

Note: The configured producers can be made available for orders within a contract.

----

<!-- .slide: data-background="#00686b" -->

![Re Panier Menu](./RePanier/repanier_menu.jpg)

Note: Menu view

----

<!-- .slide: data-background="#00686b" -->

![RePanier Producers details](./RePanier/repanier_producteurs.jpg)

Note: The balance for producers and details to contact

----

<!-- .slide: data-background="#00686b" -->

![RePanier Products](./RePanier/repanier_produits.jpg)

Note: Products view, why is the inventaire infinite?

----

<!-- .slide: data-background="#00686b" -->

![Repanier vegetable order](./RePanier/repanier_order_veg.jpg)

Note: Not single vegetables are ordered, but a box. Color codes for boxes invnted by farmer. Community builing is fostered by sharing the delivery -> 1/2 share of potatos

----

<!-- .slide: data-background="#00686b" -->

![Repanier dairy order](./RePanier/repanier_order_diary.jpg)

Note: dairy products are defined more and come less frequently

----

<!-- .slide: data-background="#00686b" -->

<div class="stretch">
![Repanier products 2](./RePanier/repanier_produits.png)
</div>

---

## [FoodCoopShop.com](https://www.foodcoopshop.com)
![foodcoopshop logo](./logos/logo_foodcoopshop.jpg)

<p class="fragment">developed by Mario Rathauer in Austria as OpenSource  
<br>
in use by ~25 foodcoops and one whole food shop  
<br>
each instance runs autonomously (has an own databse) but they can be federated using a master-slaves foodcoops model  
<br>
includes time billing functionality: Members can pay their food with working hours on the producing farm.
</p>
----

- cakePHP Applikation with node.js backend
- https://github.com/foodcoopshop/foodcoopshop
- See the comparison of [OpenFoodNetwork, FoodCoopShop and FoodSoft](https://datengarten.allmende.io/s/iHpogXeKCLZwESC )

---

## [FoodSoft](https://foodcoops.github.io/)
<p class="fragment">originally created from a member of ther Berliner foodcoop [FC Schinke 09](https://fcschinke09.de/) back in 2007  
<br>
maintenance is now maily done by wvengen, Amsterdam  
<br>
[Hosting is mainly done by hosting communities](https://foodcoops.github.io/foodsoft-hosting/) on a regional scale, or you can [deploy it yourself with docker](https://github.com/foodcoops/foodsoft/blob/master/doc/SETUP_PRODUCTION.md).  
<br>
compatibility with the article list standard of the  [Bundesverbandes Naturkost Naturwaren Großhandel e.V. (BNN)](https://n-bnn.de/sites/default/dateien/bilder/Downloads/BNN-3-Schnittstelle_01_04_2011.pdf) which allows to mass import article data from some whole salers
<br>
article database is sharable between multiple CSA instances on the same host  
<br>
See the comparison of [OpenFoodNetwork, FoodCoopShop and FoodSoft](https://datengarten.allmende.io/s/iHpogXeKCLZwESC )
</p>

---

## Discussion

----

### What is best suited for whom?

https://community.openfoodnetwork.org/t/ofn-comparison-with-similar-platforms/1191/7

Note: Very difficult question. Users have to make up their minds themselves.<br> Theresa Schumilas wrote a nice text for helping at this point.<br>Regional developments are fine, as they are made for the local variety of CSA<br>The problem is that these are often made by small teams that are quickly heavily overloaded.

----

### Easy devision of the harvest (ACP)

- ACP-Admin

----

### SEPA generation

- Sunu

----

### Individualized regular delivery (AMAP)

- cagette.net
- AMAPj

----

### Volunteer coordination

- juntagrico
- ACP-Admin
- OpenOlitor

----

### Management of shares / subscriptions Solawi & ACP

- ACP-Admin
- sunu
- juntagrico

----

### Management of a network of food hubs and producers

- OpenFoodNetwork

---

## What are we working on?

----

### How can we present all these solutions in a federated way, so consumers can find alternative food distribution systems easily?

<p class="fragment">Around the OpenFoodNetwork has already emerged a global community with strong ethical values. A [discussion is going on](https://community.openfoodnetwork.org/t/ofn-software-vs-organisation) how other food distribution systems/technologies can be integrated.</p>
<p class="fragment">Another option would be to set up a map by using a CSA mapping tool. ([Ernteteilen/teikei?](https://github.com/teikei/teikei)) This would require a not yet existing organisation for its maintenance (maybe [urgenci](https://urgenci.net/)?)</p>

----

### How do we strengthen the idea of solidarity economy within SFS through the use of technology?

<p class="fragment">Within the SolidBase project we are working on a annual budget planning and presentation tool for making the financial dimension of community supported agriculture more visible and palpable.</p>
<p class="fragment">This would be the missing piece between general management solutions and the accountancy.</p>
<p class="fragment">Strengthening the financial base of the solidarity economy might secure it from assimilation into profit orientated economy.</p>

----

### How might a network of community supported producers become thinkable while sustaining the partly decommodification of food that is happening in (smaller) CSAs?

<p class="fragment">For letting the next iteration of solidarity based economy become reality it might be necessary to use a new vocabulary like [valueflo.ws](https://www.valueflo.ws/) and rethinking the value equations for creating [Network Ressource Planning Solutions](http://wiki.p2pfoundation.net/Network_Resource_Planning).</p>
