# Presentation of digital tools suitable for administering Solidarity Based Food Systems (SFS)

## compile

This presentation is writen in markdown and is compiled with [reveal-md](https://lab.allmende.io/solidbase/website/blob/master/.gitlab-ci.yml)

## deploy

This is currently deployed on slides.solidbase.info with https://lab.allmende.io/ecobytes/lab/compose-reveal-md